<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedido".
 *
 * @property int $id
 * @property float $total
 * @property string|null $fecha
 * @property int $id_cliente
 * @property int $id_comercial
 *
 * @property Cliente $cliente
 * @property Comercial $comercial
 */
class Pedido extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedido';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total', 'id_cliente', 'id_comercial'], 'required'],
            [['total'], 'number'],
            [['fecha'], 'safe'],
            [['id_cliente', 'id_comercial'], 'integer'],
            [['id_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::class, 'targetAttribute' => ['id_cliente' => 'id']],
            [['id_comercial'], 'exist', 'skipOnError' => true, 'targetClass' => Comercial::class, 'targetAttribute' => ['id_comercial' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'total' => 'Precio Total',
            'fecha' => 'Fecha',
            'id_cliente' => 'Cliente',
            'id_comercial' => 'Comercial',
        ];
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Cliente::class, ['id' => 'id_cliente']);
    }

    /**
     * Gets query for [[Comercial]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComercial()
    {
        return $this->hasOne(Comercial::class, ['id' => 'id_comercial']);
    }
    
    public function comerciales(){
        $consulta= Comercial::find()->all(); // select * from comercial;
        $salida= \yii\helpers\ArrayHelper::map(
                    $consulta, // LISTADO DE TODOS LOS COMERCIALES
                    'id', // CAMPO QUE QUIERO ALMACENAR EN LA TABLA
                    'nombrecompleto'  // CAMPO QUE QUIERO MOSTRAR  
                );
        return $salida;
    }
    
    public function clientes(){
        $consulta= Cliente::find()->all(); // select * from cliente;
        $salida= \yii\helpers\ArrayHelper::map(
                    $consulta, //LISTADO DE TODOS LOS CLIENTES
                    'id', // CAMPO QUE ALMACENA EN LA TABLA PEDIDOS SOBRE EL CLIENTE
                    'nombrecompleto' // CAMPO DE LA TABLA CLIENTES QUE MUESTRA EN EL CONTROL
                );
        return $salida;
    }
   
}
